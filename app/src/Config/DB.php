<?php

namespace App\Config;


class DB extends \PDO
{
    static private $PDOInstance;

    public function __construct()
    {

        parent::__construct("mysql:host=" . $_ENV['DB_HOST'] . ";dbname=" . $_ENV['DB_NAME'] . ";port=3306", $_ENV['USER'], $_ENV['PASSWORD']);
    }

    public static function getInstance(): DB
    {
        if (!self::$PDOInstance) {

            self::$PDOInstance = new self();
        }
        return self::$PDOInstance;

    }
}
