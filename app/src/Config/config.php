<?php

use App\Config\DB;

return [
    'DB' => function () {
        return DB::getInstance();
    },
    'UserRepository' => DI\autowire()->constructor(DI\get('DB')),
    'AuthorisationController' => DI\autowire()->constructor(DI\get('UserRepository'))
];