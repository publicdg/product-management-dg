<?php

namespace App\Controllers;

class DashboardController extends BaseController
{

    public function index()
    {
        if (isset($_SESSION["login"]) && $_SESSION["login"] === 1) {
            return $this->render('dashboard');
        } else {
            return $this->render('/ErrorPages/notAuthorisedErrorPage');
        }
    }

}