<?php

namespace App\Controllers;


use App\Repositories\UserRepository;
use App\ValueObjects\Email;
use Exception;


class AuthorisationController extends BaseController
{

    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function authorise()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            if (!isset($_POST['email']) || !isset($_POST['password'])) {
                throw new Exception('Invalid data');
            }

            try {
                $email = new Email(htmlentities($_POST['email']));
                $password = htmlentities($_POST['password']);

                if (!$this->userRepository->doesEmailExist($email)) {
                    throw new \Exception('Invalid credentials');
                }

                $passwordHash = $this->userRepository->findPasswordHashByEmail($email);
                $isValidPassword = password_verify($password, $passwordHash);

                if (!$isValidPassword) {
                    throw new \Exception('Invalid credentials');
                }

            } catch (Exception $exception) {
                return $this->render('login', ['error' => $exception->getMessage()]);
            }
            $_SESSION["login"] = 1;
            header('Location:/app/dashboard', true, 301);
            die();

        }
        return $this->render('login', ['error' => '']);

    }

    public function logout()
    {
        if ($_SESSION['login'] === 1) {
            $_SESSION = array();
            session_destroy();
        }
        header('Location: /app/login');
        exit();
    }

}