<?php

namespace App\Controllers;

use App\Models\User;
use App\Repositories\UserRepository;
use App\ValueObjects\Email;
use App\ValueObjects\PasswordHash;
use Exception;

class RegisterController extends BaseController
{
    protected UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function create()
    {
        $this->render('register', ['error' => ""]);
    }

    public function addNewUser()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            try {

                if (!isset($_POST['email']) || !isset($_POST['password'])) {
                    throw new Exception('Invalid data!');
                }

                $email = new Email(htmlentities($_POST['email']));

                if ($this->userRepository->doesEmailExist($email)) {
                    throw new Exception('The email already exists. Please login.');
                }
                $passwordHash = new PasswordHash(htmlentities($_POST['password']));

            } catch (Exception $exception) {
                return $this->render('register', ['error' => $exception->getMessage()]);
            }

            $user = new User($email, $passwordHash);
            $this->userRepository->createNewUser($user);
        }
        return $this->render('login', ['error' => ""]);
    }

}