<?php

namespace App\ValueObjects;

class StringVO
{
    protected string $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }

}