<?php

namespace App\ValueObjects;

class PasswordHash extends StringVO
{
    public function __construct(string $value)
    {
        if (!(strlen($value) > 7)) {

            throw new \Exception('The password should be at lest 8 characters long');
        }
        $this->value = password_hash($value, PASSWORD_ARGON2_DEFAULT_THREADS);
    }

}