<?php

namespace App\ValueObjects;

class Email extends StringVO
{

    public function __construct(string $value)
    {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Invalid email');
        }

        $this->value = $value;
    }

}