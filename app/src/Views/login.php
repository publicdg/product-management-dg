<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            document.querySelector('form').addEventListener('submit', (event) => {
                event.preventDefault();

                let password = $('#password').val();
                let email = $('#email').val();

                $.ajax({
                    url: "/app/login",
                    dataType: 'text',
                    async: true,
                    type: 'POST',
                    data: {email: email, password: password},
                    success: function (data, status) {
                        $('body').html(data);
                    },
                    error: function (xhr, textStatus, errorThrown) {

                    }
                });
            });
        });

    </script>

</head>

<body>
<div class="container">
    <h1>Login</h1>

    <form method="POST">
        <div class="form-group">
            <label for="email">Email</label>
            <div class="input-group">
                <label>
                    <input id="email" name="email" type="email">
                </label>
            </div>
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <div class="input-group">
                <label>
                    <input id="password" name="password" type="password">
                </label>
            </div>
        </div>
        <button style="margin-top: 15px" type="submit" class="btn btn-success">
            Submit
        </button>
        <div id="message"><?= $error ?></div>
    </form>
    <a href="/app/register">Register</a>
</div>


</body>

</html>
