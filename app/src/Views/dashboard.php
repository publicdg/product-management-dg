<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
</head>

<script type="text/javascript">
    $(document).ready(function () {
        window.history.replaceState(null, null, "/app/dashboard");
        return false;
    });
</script>
<body>
<div class="container">
    <h1>Welcome</h1>
    <div>Content of dashboard</div>
    <a href="/app/logout" style="margin-top: 15px" class="btn btn-info" role="button">Logout</a>

</div>

</body>

</html>
