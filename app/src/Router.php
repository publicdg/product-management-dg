<?php

namespace App;

class Router
{

    protected array $routes = [];

    public function addRoute($route, $controller, $action): void
    {
        $this->routes[$route] = ['controller' => $controller, 'action' => $action];

    }

    public function dispatch($uri, $container): void
    {

        if (array_key_exists($uri, $this->routes)) {
            $controller = $this->routes[$uri]['controller'];
            $action = $this->routes[$uri]['action'];


            $controller = $container->get($controller);
            $controller->$action();
        } else {
            header("HTTP/1.0 404 Not Found");
            echo file_get_contents(__DIR__ . "/Views/ErrorPages/RouteNotFoundErrorPage.php");
        }
    }

}
