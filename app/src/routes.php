<?php

use App\Controllers\AuthorisationController;
use App\Controllers\DashboardController;
use App\Controllers\RegisterController;
use App\Router;

$router = new Router();

$router->addRoute('/app/register', RegisterController::class, 'create');
$router->addRoute('/app/login', AuthorisationController::class, 'authorise');
$router->addRoute('/app/', AuthorisationController::class, 'authorise');
$router->addRoute('/app/logout', AuthorisationController::class, 'logout');
$router->addRoute('/app/add-user', RegisterController::class, 'addNewUser');
$router->addRoute('/app/dashboard', DashboardController::class, 'index');
