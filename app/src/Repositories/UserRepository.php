<?php

namespace App\Repositories;

use App\Config\DB;
use App\Models\User;
use App\ValueObjects\Email;

class UserRepository
{

    private $dbInstance;

    public function __construct(DB $dbInstance)
    {
        global $conf;
        $this->dbInstance = $dbInstance;
        //$this->dbInstance = DB::getInstance();
    }

    public function createNewUser(User $user): void
    {
        $sql = <<<SQL
INSERT INTO user (email, password)
VALUES (:email, :password)
SQL;

        $stm = $this->dbInstance->prepare($sql);

        $stm->bindValue(':email', $user->getEmail()->getValue());
        $stm->bindValue(':password', $user->getPassword()->getValue());

        $stm->execute();
    }

    public function isAuthorisedUser(User $user): bool
    {

        $sql = <<<SQL
SELECT * FROM user WHERE email = :email AND password = :password
SQL;

        $stm = $this->dbInstance->prepare($sql);

        $stm->bindValue(':email', $user->getEmail()->getValue());
        $stm->bindValue(':password', $user->getPassword()->getValue());

        $stm->execute();

        if ($stm->rowCount() < 1) {
            return false;
        }

        return true;
    }

    public function doesEmailExist(Email $email): bool
    {
        $sql = <<<SQL
SELECT * FROM user WHERE email = :email
SQL;

        $stm = $this->dbInstance->prepare($sql);

        $stm->bindValue(':email', $email->getValue());

        $stm->execute();

        if ($stm->rowCount() < 1) {
            return false;
        }

        return true;

    }

    public function findPasswordHashByEmail(Email $email): ?string
    {

        $sql = <<<SQL
SELECT password FROM user WHERE email = :email
SQL;

        $stm = $this->dbInstance->prepare($sql);

        $stm->bindValue(':email', $email->getValue());

        $stm->execute();

        if ($stm->rowCount() < 1) {
            return null;
        }

        return $stm->fetchColumn();
    }


}