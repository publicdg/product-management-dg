<?php

namespace App\Models;

use App\ValueObjects\Email;
use App\ValueObjects\PasswordHash;

class User
{

    private Email $email;
    private PasswordHash $password;

    public function __construct(Email $email, PasswordHash $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getPassword(): PasswordHash
    {
        return $this->password;
    }


}