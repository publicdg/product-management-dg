<?php
require __DIR__ . "/vendor/autoload.php";
require __DIR__ . '/src/routes.php';

use DI\ContainerBuilder;


use Dotenv\Dotenv;

ob_start();
session_start();

global $router;

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

$containerBuilder = new ContainerBuilder();
$containerBuilder->addDefinitions(
    __DIR__ . '/src/Config/config.php'
);

$container = $containerBuilder->build();

$uri = $_SERVER['REQUEST_URI'];

$router->dispatch($uri, $container);
ob_end_flush();
exit();